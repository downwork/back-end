package ro.ubb.downWork.profilemicro.model;

import lombok.*;

import javax.persistence.*;

/**
 * Created by langchristian96 on 10/20/2017.
 */


@Entity

@Table(name = "person",uniqueConstraints = {
        @UniqueConstraint(columnNames = "username")
})
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@Inheritance(strategy = InheritanceType.JOINED)
public class Person extends BaseEntity<Long> {

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "picture", nullable = false)
    private String picture;

    @Column(name = "location", nullable = false)
    private String location;


}
