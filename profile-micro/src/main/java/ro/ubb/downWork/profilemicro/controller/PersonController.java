package ro.ubb.downWork.profilemicro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.*;
import ro.ubb.downWork.profilemicro.dto.PersonDto;
import ro.ubb.downWork.profilemicro.model.Person;
import ro.ubb.downWork.profilemicro.service.PersonService;

import java.util.List;

/**
 * Created by langchristian96 on 10/20/2017.
 */
@RestController
public class PersonController {


    @Autowired
    private PersonService personService;


    @RequestMapping(value = "/persons", method = RequestMethod.GET)
    @CrossOrigin
    public List<Person> getPersons() {          //TODO: change into PersonsDto

        return personService.findAll();
    }



    @RequestMapping(value = "/persons/{usern}", method = RequestMethod.GET)
    @CrossOrigin

    public PersonDto getPersonByUsername(@PathVariable final String usern) {
        Person person = personService.getByUserName(usern);
        PersonDto personDto = PersonDto.builder()
                .username(person.getUsername())
                .password(person.getPassword())
                .location(person.getLocation())
                .picture(person.getPicture())
                .build();
        personDto.setId(person.getId());
        return personDto;
    }


}
