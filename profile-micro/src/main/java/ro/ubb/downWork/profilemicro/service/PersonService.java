package ro.ubb.downWork.profilemicro.service;

import ro.ubb.downWork.profilemicro.model.Person;

import java.util.List;

/**
 * Created by langchristian96 on 10/20/2017.
 */
public interface PersonService {

    List<Person> findAll();

    Person getByUserName(String userName);

}
