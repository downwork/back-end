package ro.ubb.downWork.profilemicro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.downWork.profilemicro.model.Person;
import ro.ubb.downWork.profilemicro.repository.PersonRepository;

import java.util.List;

/**
 * Created by langchristian96 on 10/20/2017.
 */
@Service
public class PersonServiceImpl implements PersonService {


    @Autowired
    private PersonRepository personRepository;

    @Override
    public List<Person> findAll() {
        return (List<Person>)personRepository.findAll();
    }

    @Override
    public Person getByUserName(String userName) {
            return personRepository.getByUserName(userName);
    }
}
