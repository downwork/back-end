package ro.ubb.downWork.profilemicro.dto;

import lombok.*;

/**
 * Created by langchristian96 on 10/20/2017.
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PersonDto extends BaseEntityDto {
    private String username;
    private String password;
    private String picture;
    private String location;

    @Override
    public String toString() {
        return "PersonDto{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", picture='" + picture + '\'' +
                ", location='" + location + '\'' +
                '}'+super.toString();
    }
}

